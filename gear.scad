use <Write.scad>

//User Defined Variables
gearRadius = 40;
toothHeight = 5;
numberOfGearTeeth = 20;
taper = 3;

//Private Variables DO NOT EDIT
toothWidth = (3.1415 * gearRadius) / numberOfGearTeeth - taper;
color("red")
{
    translate([-20,0,0])
        write("Hailey",t=5,h=10);
}

linear_extrude(height = 4)
{
    difference()
    {
        union()
        {
            circle(r=gearRadius);
            for(i= [ 0: numberOfGearTeeth - 1])
            {
                rotate ( [0,0,i * 360 / numberOfGearTeeth ])
                
                        polygon(points = [
                              [toothWidth / 2, gearRadius + toothHeight],
                              [taper+toothWidth / 2, gearRadius - 1],
                              [-taper-toothWidth / 2, gearRadius - 1],
                              [-toothWidth / 2, gearRadius + toothHeight]]);
            }
        }
        
        hull()
        {
            translate([gearRadius * .25,gearRadius * .75])
                circle(r = 2.5);
            translate([-gearRadius * .25,gearRadius * .75])
                circle(r = 2.5);
        }
    }
}